# FSSE Dev Container

To do this, you need [Docker](https://docker.com), [VS Code](visualstudio.com) and the [VS Code Remote Extension](https://marketplace.visualstudio.com/items?itemName=ms-vscode-remote.remote-containers).

## Getting it running

Clone the repo:

```
git clone https://gitlab.com/fullstack-bootcamp/bootcamp-1-2020/fsse-bootcamp-content/development-environment
```

Then `cmd+shift+P` and type `Remote Container` and select `Open Folder in Remote Container` and select the folder you just cloned (`development-environment`). If prompted again, choose the `dev` option.

It will take some time as it builds the images.

Once complete, open a VS Code terminal, and you should see the prompt that looks something like: `root@efc138a1cadf:/workspace`.

#### Continual additions
This development environment will be added to as we go through the bootcamp, so when there are additions (you will notified via slack or in lecture), make sure to do a:

```
git checkout [branch]
git pull origin [branch]
```
where `[branch]` is provided by the instructor.

Don't worry, the containerized environment is designed to not wipe out any of your development, it will only add to the existing container setups.

VSCode will detect when there are changes to the compose or dockerfiles, and will prompt you to rebuild the containers.  

## Suggested Setup
It is suggested to create a folder called `/projects` in the workspace:

```
mkdir projects
```

In there, you can do all your development, including keeping your separate repositories and pulling the weekly starter repos as provided.


## Using it with SSH
In the event that you do not have a machine that supports Docker, there is the option to set up a small ($10) Ubuntu 20.04 Droplet on Digital Ocean (https://digitalocean.com).  Once you log into the VM, you should install docker:

```
apt update
apt install docker.io
```

and clone the dev env:
```
git clone https://gitlab.com/fullstack-bootcamp/bootcamp-1-2020/fsse-bootcamp-content/development-environment
```

and then set it up:

```
cd development-environment
docker-compose up
```

This will build the container and you can SSH into it from your VS code using the VS code SSH extension.

> You will likely need help with each one of these steps...