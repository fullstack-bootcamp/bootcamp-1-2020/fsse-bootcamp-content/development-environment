import subprocess
#

from http.server import HTTPServer, BaseHTTPRequestHandler
import json

# open json file and give it to data variable as a dictionary

processes = {}


# Defining a HTTP request Handler class
class ServiceHandler(BaseHTTPRequestHandler):
    # sets basic headers for the server
    def _set_headers(self):
        self.send_response(200)
        self.send_header('Content-type', 'application/json')
        # reads the length of the Headers
        length = int(self.headers['Content-Length'])
        # reads the contents of the request
        content = self.rfile.read(length)
        temp = str(content).strip('b\'')
        self.end_headers()
        return temp

    ######
    #LIST#
    ######
    # GET Method Defination
    def do_GET(self):
        # defining all the headers
        self.send_response(200)
        self.send_header('Content-type', 'application/json')
        self.end_headers()
        pp = None
        keys = []
        p = self.path.split("?")

        if p[0] == "/ids" or p[0] == "/ids/":
            pass

        if p[0] == "/stop" or p[0] == "/stop/":

            keys = list(processes.keys())
            for process in keys:
                processes[process].kill()
                del processes[process]
            # subprocess.Popen(["kill", id], stdout=subprocess.PIPE)
        if p[0] == "/start" or p[0] == "/start/":

            vars = p[1].split("&")
            config = "/workspace/sumo/example/first_network.sumocfg"
            for k in vars:
                pair = k.split("=")
                if pair[0] == "config":
                    config = pair[1]
                    print("config: ", config)

            pp = subprocess.Popen(["sumo",
                                   "-c", config,
                                   "--remote-port", "8814"], stdout=subprocess.PIPE)
            processes[pp.pid] = pp
            keys = processes.keys()

        keys = processes.keys()
        self.wfile.write(json.dumps(
            {"status": "ok", "id": list(keys)}).encode())


# Server Initialization
server = HTTPServer(('0.0.0.0', 11920), ServiceHandler)
server.serve_forever()
