
import sys
import os
from time import sleep
# export SUMO_HOME=/usr/local/Cellar/sumo/1.6.0/
tools = os.path.join(os.environ['SUMO_TOOLS'])
sys.path.append(tools)

import traci

# Assumes `sumo-gui` is on the path already.  If not, you will need to update it to include the fully qualified path to your sumo-gui binary
# sumoBinary = "sumo"
# sumoCmd = [sumoBinary, "-c", "first_network.sumocfg", "--start", "--time-to-teleport", "100000000"]

# traci.start(sumoCmd)

traci.init(port=8814, numRetries=10, host="sumoservice")

print(traci.vehicle.getSpeed("veh0"))
step = 1
while step < 10000:
    traci.simulationStep()
    speed = traci.vehicle.getSpeed("veh0")
    print(speed)
    
    current_road = traci.vehicle.getRoadID("veh0")
    
    if current_road == "top_east":
        traci.vehicle.setStop("veh0", "east_south", duration=30)
    
    
#     step += 1
#     sleep(0.5)
    
    
print("done!")